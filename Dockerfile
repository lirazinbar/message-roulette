FROM node:14-alpine as BUILD
WORKDIR /app
RUN echo "@bit:registry=https://node.bit.dev" >> ~/.npmrc
RUN echo "//node.bit.dev/:_authToken={$BIT_TOKEN}" >> ~/.npmrc
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build

FROM node:14-alpine as PROD 
WORKDIR /app
RUN echo "@bit:registry=https://node.bit.dev" >> ~/.npmrc
RUN echo "//node.bit.dev/:_authToken={$BIT_TOKEN}" >> ~/.npmrc
COPY --from=BUILD /app/package*.json ./
COPY --from=BUILD app/dist ./dist
RUN npm run install:prod
ENTRYPOINT ["node", "/app/dist/index.js"]
EXPOSE 3000

