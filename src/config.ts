import * as env from 'env-var';

export const config = {  
  authentication: {
    tokenKey: env.get('TOKEN_KEY').default('secret').asString(),
  },
  db: {
    connectionURL: env
      .get('DB_CONNECTION_URL')
      .default('mongodb://localhost:27017/message-roulette')
      .asString(),
  },
  redis: {
    port: env.get('REDIS_PORT').default(6379).asPortNumber(),
    host: env.get('REDIS_HOST').default('localhost').asString(),
    names: {
      connectedUsers: env.get('REDIS_CONNECTED_USERS').default('connected-users').asString(),
    },
    folders: {
      userSockets: env.get('REDIS_FOLDER_USER_SOCKETS').default('user-sockets').asString(),
      socketUser: env.get('REDIS_FOLDER_SOCKET_USER').default('socket-user').asString(),
    },
  },
  server: {
    port: env.get('APPLICATION_PORT').default(8080).asPortNumber(),
    name: 'message-roulette service',
  },
};
