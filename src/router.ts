import { Router } from 'express';
import { MessageRouletteRouter } from './messageRoulette/messageRoulette.router';

const AppRouter: Router = Router();

AppRouter.use(MessageRouletteRouter);
AppRouter.use('/isAlive', (req, res) => {
  res.status(200).send('alive');
});
AppRouter.use('*', (req, res) => {
  res.status(404).send('Invalid Route');
});

export { AppRouter };