import * as mongoose from 'mongoose';

export const UserSchema: mongoose.Schema = new mongoose.Schema(
  {
    personalId: {
      type: String,
      required: true,
      unique: true,
    },
    token: {
      type: String,
      required: true,
      unique: true,
    },
    createdAt: {
      type: Date,
      default: Date.now,
    }
  },
  {
    timestamps: true,
    id: true,
    toJSON: {
      virtuals: true,
      transform(doc, ret) {
        const returnedValue = ret;
        delete returnedValue._id;
      },
    },
    discriminatorKey: 'type',
  },
);

export const UserModel = mongoose.model<{ personalId: string; token: string; } & mongoose.Document>('User', UserSchema);

// set expiration after 15 minutes
UserSchema.index({ createdAt: 1 }, { expireAfterSeconds: 900 });

UserModel.ensureIndexes();

