import { Router } from 'express';
import { MessageRouletteController } from './messageRoulette.controller';
import { wrapAsync } from '../utils/wrapper';
import { validateMiddleware } from './messageRoulette.validator';
import { sendMessageToXUsersSchema, createTokenSchema, sendMessageSchema } from './messageRoulette.schema';
import { verifyToken } from '../utils/authentication';

const MessageRouletteRouter: Router = Router();

MessageRouletteRouter.post('/spin', verifyToken, validateMiddleware(sendMessageSchema), wrapAsync(MessageRouletteController.sendMessageToRandomUser));
MessageRouletteRouter.post('/wild', verifyToken, validateMiddleware(sendMessageToXUsersSchema), wrapAsync(MessageRouletteController.sendMessageToXUsers));
MessageRouletteRouter.post('/blast', verifyToken, validateMiddleware(sendMessageSchema), wrapAsync(MessageRouletteController.sendMessageToAllUsers));
MessageRouletteRouter.post('/create-session-token', validateMiddleware(createTokenSchema), wrapAsync(MessageRouletteController.createSessionToken));

export { MessageRouletteRouter };


