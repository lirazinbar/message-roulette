import { Request, Response } from 'express';
import { ConnectedUserManager } from '../user/connectedUser/connectedUser.manager';
import * as _ from 'lodash';
import { SocketEmitter } from '../socket/socket.emitter';
import * as jwt from 'jsonwebtoken';
import { UserModel } from './user.model';
import { config } from '../config';
import { MessageRouletteManager } from './messageRoulette.manager';

export class MessageRouletteController {
  static async sendMessageToRandomUser(req: Request, res: Response) {
    const connectedUsers: string[] = await ConnectedUserManager.getConnectedUsers();
    const randomUser = _.sample(connectedUsers);

    if (randomUser)
      SocketEmitter.emitMessageToUsers({ eventType: "MESSAGE", content: `Hello user *SPIN* - ${randomUser}!` }, [randomUser]);
    res.json({});
  }

  static async sendMessageToXUsers(req: Request, res: Response) {
    const connectedUsers: string[] = await ConnectedUserManager.getConnectedUsers();
    const randomUsers = _.sampleSize(connectedUsers, req.body.x);

    SocketEmitter.emitMessageToUsers({ eventType: "MESSAGE", content: `Hello users! *WILD*` }, randomUsers);
    res.json({});
  }

  static async sendMessageToAllUsers(req: Request, res: Response) {
    SocketEmitter.emitMessageToUsers({ eventType: "MESSAGE", content: `Hello users! *BLAST*` }, [], true);
    res.json({});
  }

  static async createSessionToken(req: Request, res: Response) {
    const token = jwt.sign({ personalId: req.body.personalId }, config.authentication.tokenKey, { expiresIn: '900s' });
    const existUser = await MessageRouletteManager.getByPersonalId(req.body.personalId);

    if (existUser)
      res.json(await MessageRouletteManager.updateTokenByPersonalId(req.body.personalId, token));
    else
      res.json(await MessageRouletteManager.create(req.body.personalId, token));
  }
}
