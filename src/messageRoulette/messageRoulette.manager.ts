import { MessageRouletteRepository } from "./messageRoulette.repository";

export class MessageRouletteManager {
    static getByPersonalId(personalId: string) {
        return MessageRouletteRepository.getByPersonalId(personalId);
    }

    static updateTokenByPersonalId(personalId: string, token: string) {
        return MessageRouletteRepository.updateTokenByPersonalId(personalId, token);
    }

    static create(personalId: string, token: string) {
        return MessageRouletteRepository.create(personalId, token);
    }

    static deleteByPersonalId(personalId: string) {
        return MessageRouletteRepository.deleteByPersonalId(personalId);
    }
}