import { UserModel } from "./user.model";

export class MessageRouletteRepository {
    static getByPersonalId(personalId: string) {
        return UserModel.findOne({ personalId });
    }

    static updateTokenByPersonalId(personalId: string, token: string) {
        return UserModel.findOneAndUpdate({ personalId }, { token }, { new: true });
    }

    static create(personalId: string, token: string) {
        return UserModel.create({ token, personalId });
    }

    static deleteByPersonalId(personalId: string) {
        return UserModel.findOneAndDelete({ personalId });
    }
}