import * as Joi from 'joi';

export const sendMessageToXUsersSchema = Joi.object({
  body: {x: Joi.number().min(0).required()},
  params: {},
  query: {},
});

export const sendMessageSchema = Joi.object({
  body: {},
  params: {},
  query: {},
});

export const createTokenSchema = Joi.object({
  body: {personalId: Joi.string().required()},
  params: {},
  query: {},
});
