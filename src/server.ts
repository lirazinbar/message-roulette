import * as express from 'express';
import * as http from 'http';
import * as helmet from 'helmet';
import * as morgan from 'morgan';
import * as winston from 'winston';
import * as expressWinston from 'express-winston';
import { config } from './config';
import { AppRouter } from './router';
import { SocketListener } from './socket/socket.listener';

export class Server {
  public app: express.Application;

  public ioSocket!: any;

  private httpServer: http.Server;

  public static startServer(): Server {
    return new Server();
  }

  public close() {
    this.httpServer.close();
  }

  private constructor() {
    this.app = express();    
    this.app.use(express.json());
    this.app.use(express.urlencoded({ extended: true }));    
    this.app.use(AppRouter);
    this.configurationMiddleware();
    this.initializeErrorHandler();
    const httpConnection = new http.Server(this.app);
    this.httpServer = httpConnection.listen(config.server.port, () => {
      console.log(
        `${config.server.name} listening on port ${config.server.port}`,
      );
    });
    SocketListener.initSocketHandler(this.httpServer).catch((err) => {
      console.log('Socket Listener Error');
      process.exit(1);
    });
  }

  private setLogger = (): express.Handler => expressWinston.logger({
    transports: [new winston.transports.Console()],
    meta: true,
    expressFormat: true,
    skip: () => {
      return true;
    },
  });

  private setHeaders = (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Credentials', 'true');
    res.setHeader('Access-Control-Allow-Methods', 'GET');
    res.setHeader(
      'Access-Control-Allow-Headers',
      'Authorization, Origin, X-Requested-With, Content-Type',
    );
    next();
  };

  private configurationMiddleware() {
    this.app.use(helmet());
    this.app.use(this.setHeaders);
    this.app.use(this.setLogger());
    this.app.use(morgan('dev'));
  }

  private initializeErrorHandler() {
    this.app.use(userErrorHandler);
    this.app.use(unknownErrorHandler);
  }
}
function userErrorHandler(userErrorHandler: any) {
  throw new Error('Function not implemented.');
}

function unknownErrorHandler(unknownErrorHandler: any) {
  throw new Error('Function not implemented.');
}

