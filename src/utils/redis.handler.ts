import * as redis from 'redis';
import * as util from 'util';
import * as Server from 'socket.io';
import { config } from '../config';

export class RedisHandler {
  static client: redis.RedisClient;

  static promisifiedCommands: Record<string, Function> = {};

  static async connect(dbIndex = 0) {
    this.client = await redis.createClient(config.redis.port, config.redis.host);
    this.client = await this.setDb(dbIndex);
    this.buildPromisifiedCommands();
  }

  static buildPromisifiedCommands() {
    this.promisifiedCommands.set = util.promisify(this.client.set).bind(this.client);
    this.promisifiedCommands.get = util.promisify(this.client.get).bind(this.client);
    this.promisifiedCommands.sismember = util.promisify(this.client.sismember).bind(this.client);
    this.promisifiedCommands.smembers = util.promisify(this.client.smembers).bind(this.client);
    this.promisifiedCommands.sadd = util.promisify(this.client.sadd).bind(this.client);
    this.promisifiedCommands.srem = util.promisify(this.client.srem).bind(this.client);
    this.promisifiedCommands.flushdb = util.promisify(this.client.flushdb).bind(this.client);
    this.promisifiedCommands.del = util.promisify(this.client.del).bind(this.client);
    this.promisifiedCommands.scard = util.promisify(this.client.scard).bind(this.client);
  }

  static setDb(dbIndex: number): Promise<redis.RedisClient> {
    return new Promise((resolve, reject) => {
      this.client.select(dbIndex, (err: Error | null) => {
        if (err) reject(err);
        resolve(this.client);
      });
    });
  }

  static setRecord(key: string, value: string) {
    return this.promisifiedCommands.set(key, value);
  }

  static getValue(key: string) {
    return this.promisifiedCommands.get(key);
  }

  static checkIfMemberExists(setName: string, member: string) {
    return this.promisifiedCommands.sismember(setName, member);
  }

  static getSetById(setName: string) {
    return this.promisifiedCommands.smembers(setName);
  }

  static addMemberToSet(setName: string, member: string) {
    return this.promisifiedCommands.sadd(setName, member);
  }

  static joinSocketToRoom(roomName: string, socket: Server.Socket) {
    return socket.join(roomName);
  }

  static leaveSocketFromRoom(roomName: string, socket: Server.Socket) {
    return socket.leave(roomName);
  }

  static deleteValueFromSet(setName: string, member: string) {
    return this.promisifiedCommands.srem(setName, member);
  }

  static clearAllDb() {
    return this.promisifiedCommands.flushdb();
  }

  static deleteRecordByKey(key: string) {
    return this.promisifiedCommands.del(key);
  }

  static getSetLength(setName: string) {
    return this.promisifiedCommands.scard(setName);
  }
}
