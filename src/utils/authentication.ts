import { Request, Response } from 'express';
import * as jwt from 'jsonwebtoken';
import { config } from '../config';
import { MessageRouletteManager } from '../messageRoulette/messageRoulette.manager';
import { UserModel } from '../messageRoulette/user.model';

// check token validation
export const verifyToken = async (req: Request, res: Response, next: any) => {
    const token = req.headers.authorization?.split(" ")[1];

    if (!token) {
        return res.status(403).send("A token is required for authentication");
    }
    try {
        const decoded = jwt.verify(token as string, config.authentication.tokenKey);
        const existUser = await MessageRouletteManager.getByPersonalId((decoded as any).personalId);

        if (!existUser || existUser.token !== token)
            return res.status(401).send("Invalid Token");
    } catch (err) {
        return res.status(401).send("Invalid Token");
    }
    return next();
};
