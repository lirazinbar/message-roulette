/* eslint-disable object-curly-newline */
import * as mongoose from 'mongoose';
import { Server } from './server';
import { config } from './config';
import { RedisHandler } from './utils/redis.handler';

const initializeMongo = async () => {
  mongoose.connection.on('connecting', () => {
    console.log('[MongoDB] connecting...');
  });

  mongoose.connection.on('connected', () => {
    console.log('[MongoDB] connected');
  });

  mongoose.connection.on('error', (error) => {
    console.log('[MongoDB] error');
    process.exit(1);
  });

  mongoose.connection.on('disconnected', () => {
    console.log('[MongoDB] disconnected');
    process.exit(1);
  });

  await mongoose.connect(config.db.connectionURL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });
};

const main = async () => {
  process.on(
    'unhandledRejection',
    (reason: {} | null | undefined, promise: Promise<any>) => {
      console.log(
        `Unhandled Rejection at: Promise, ${promise}, reason: ${reason}`,
      );
      process.exit(1);
    },
  );

  await initializeMongo();

  await RedisHandler.connect().catch(() => 'error connecting to Redis');
  Server.startServer();
};

main().catch((error) => console.error(error.message));
