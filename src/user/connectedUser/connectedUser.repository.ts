import { config } from '../../config';
import { RedisHandler } from '../../utils/redis.handler';

const CONNECTED_USERS_FOLDER_NAME = config.redis.names.connectedUsers;

export class ConnectedUserRepository {
  static getConnectedUsers() {
    return RedisHandler.getSetById(CONNECTED_USERS_FOLDER_NAME);
  }

  static addUserId(userId: string) {
    return RedisHandler.addMemberToSet(CONNECTED_USERS_FOLDER_NAME, userId);
  }

  static deleteUserId(userId: string) {
    return RedisHandler.deleteValueFromSet(CONNECTED_USERS_FOLDER_NAME, userId);
  }
}
