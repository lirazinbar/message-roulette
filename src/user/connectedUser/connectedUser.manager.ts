import { ConnectedUserRepository } from './connectedUser.repository';

export class ConnectedUserManager {
  static getConnectedUsers() {
    return ConnectedUserRepository.getConnectedUsers();
  }

  static addUserId(userId: string) {
    return ConnectedUserRepository.addUserId(userId);
  }

  static deleteUserId(userId: string) {
    return ConnectedUserRepository.deleteUserId(userId);
  }
}
