import { RedisHandler } from '../../utils/redis.handler';
import { config } from '../../config';

const SCOKET_USER_FOLDER_NAME = config.redis.folders.socketUser;

export class SocketUserPairRepository {
  static setUserIdBySocketId(socketId: string, userId: string) {
    return RedisHandler.setRecord(`${SCOKET_USER_FOLDER_NAME}:${socketId}`, userId);
  }

  static getUserBySocketId(socketId: string) {
    return RedisHandler.getValue(`${SCOKET_USER_FOLDER_NAME}:${socketId}`);
  }

  static deleteUserIdBySocketId(socketId: string) {
    return RedisHandler.deleteRecordByKey(`${SCOKET_USER_FOLDER_NAME}:${socketId}`);
  }
}
