import { SocketUserPairRepository } from './socketUserPair.repository';

export class SocketUserPairManager {
  static setUserIdBySocketId(socketId: string, userId: string) {
    return SocketUserPairRepository.setUserIdBySocketId(socketId, userId);
  }

  static getUserBySocketId(socketId: string) {
    return SocketUserPairRepository.getUserBySocketId(socketId);
  }

  static deleteUserIdBySocketId(socketId: string) {
    return SocketUserPairRepository.deleteUserIdBySocketId(socketId);
  }
}
