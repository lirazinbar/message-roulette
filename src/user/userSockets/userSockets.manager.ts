import * as Server from 'socket.io';
import { UserSocketsRepository } from './userSockets.repository';

export class UserSocketsManager {
  static addSocket(userId: string, socketToAdd: Server.Socket) {
    return UserSocketsRepository.addSocket(userId, socketToAdd);
  }

  static deleteSocket(userId: string, socketToRemove: Server.Socket) {
    return UserSocketsRepository.deleteSocket(userId, socketToRemove);
  }

  static getSocketsAmount(userId: string) {
    return UserSocketsRepository.getSocketsAmount(userId);
  }
}
