import * as Server from 'socket.io';
import { RedisHandler } from '../../utils/redis.handler';
import { config } from '../../config';

const USER_SOCKETS_FOLDER_NAME = config.redis.folders.userSockets;

export class UserSocketsRepository {
  static async addSocket(userId: string, socketToAdd: Server.Socket) {
    await RedisHandler.joinSocketToRoom(userId, socketToAdd);
    return RedisHandler.addMemberToSet(`${USER_SOCKETS_FOLDER_NAME}:${userId}`, socketToAdd.id);
  }

  static async deleteSocket(userId: string, socketToRemove: Server.Socket) {
    await RedisHandler.leaveSocketFromRoom(userId, socketToRemove);
    return RedisHandler.deleteValueFromSet(`${USER_SOCKETS_FOLDER_NAME}:${userId}`, socketToRemove.id);
  }

  static getSocketsAmount(userId: string) {
    return RedisHandler.getSetLength(`${USER_SOCKETS_FOLDER_NAME}:${userId}`);
  }
}
