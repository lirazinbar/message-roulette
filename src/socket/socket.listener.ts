import { Server } from 'socket.io';
import { createAdapter } from 'socket.io-redis';
import { RedisClient } from 'redis';
import * as http from 'http';
import { SocketManager } from './socket.manager';
import { config } from '../config';
import { SocketEmitter } from './socket.emitter';

export class SocketListener {
  static async initSocketHandler(httpServer: http.Server) {
    const io = new Server(httpServer, {
      cors: {
        origin: '*',
        methods: ['GET'],
      },
    });

    const { host, port } = config.redis;
    const pubClient = new RedisClient({ host, port });
    const subClient = pubClient.duplicate();

    io.adapter(createAdapter({ pubClient, subClient }));
    SocketEmitter.init(io);

    io.on('connection', async (socket: any) => {
      socket.on('REGISTER', async (arg: any) => {
        await SocketManager
          .saveUserDataAfterConnecting(socket, arg.userId || arg);
      });

      socket.on('disconnect', async () => {
        await SocketManager.disconnectUser(socket);
      });
    });
  }
}
