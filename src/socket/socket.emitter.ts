import { Server } from 'socket.io';

export class SocketEmitter {
  static socketIo: Server;

  static init(socketIo: Server) {
    this.socketIo = socketIo;
  }

  static emitMessageToUsers(message: any, users: string[], broadcast = false) {
    if (broadcast) this.socketIo.emit(message.eventType, message.content);
    else this.socketIo.to(users).emit(message.eventType, message.content);
  }
}
