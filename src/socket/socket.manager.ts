import * as Server from 'socket.io';
import { UserSocketsManager } from '../user/userSockets/userSockets.manager';
import { ConnectedUserManager } from '../user/connectedUser/connectedUser.manager';
import { SocketUserPairManager } from '../user/socketUserPair/socketUserPair.manager';
import { UserModel } from '../messageRoulette/user.model';
import { MessageRouletteManager } from '../messageRoulette/messageRoulette.manager';

export class SocketManager {
  static async saveUserDataAfterConnecting(socket: Server.Socket, userId: string) {
    const existUserOnSocket = await SocketUserPairManager.getUserBySocketId(socket.id);
    // if user another user is already connect to this socket delete him
    if (existUserOnSocket) {
      await UserSocketsManager.deleteSocket(existUserOnSocket, socket);
      const socketsAmountOfUser = await UserSocketsManager.getSocketsAmount(existUserOnSocket);

      if (socketsAmountOfUser === 0) {
        await ConnectedUserManager.deleteUserId(existUserOnSocket);
        await MessageRouletteManager.deleteByPersonalId(userId);
      }

      await SocketUserPairManager.deleteUserIdBySocketId(socket.id);
    }
    return Promise.all([
      SocketUserPairManager.setUserIdBySocketId(socket.id, userId),
      UserSocketsManager.addSocket(userId, socket),
      ConnectedUserManager.addUserId(userId),
    ]);
  }

  static async disconnectUser(socket: Server.Socket) {
    const userId = await SocketUserPairManager.getUserBySocketId(socket.id) as string;

    if (!userId) {
      console.log('User to disconnect not found');
      return;
    }

    await UserSocketsManager.deleteSocket(userId as string, socket);
    const socketsAmountOfUser = await UserSocketsManager.getSocketsAmount(userId);

    if (socketsAmountOfUser === 0) {
      await ConnectedUserManager.deleteUserId(userId);
      // delete user token
      await MessageRouletteManager.deleteByPersonalId(userId);
    }

    await SocketUserPairManager.deleteUserIdBySocketId(socket.id);
  }
}
