# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [0.1.0](https://gitlab.com/yesodot/selenium/censorship-systems/blue-moon/blue-moon-real-time/compare/v0.0.7...v0.1.0) (2022-01-04)

### [0.0.7](https://gitlab.com/yesodot/selenium/censorship-systems/blue-moon/blue-moon-real-time/compare/v0.0.6...v0.0.7) (2021-12-23)

### [0.0.6](https://gitlab.com/yesodot/selenium/censorship-systems/blue-moon/blue-moon-real-time/compare/v0.0.5...v0.0.6) (2021-12-23)

### [0.0.5](https://gitlab.com/yesodot/selenium/censorship-systems/blue-moon/blue-moon-real-time/compare/v0.0.4...v0.0.5) (2021-11-25)

### [0.0.4](https://gitlab.com/yesodot/selenium/censorship-systems/blue-moon/blue-moon-real-time/compare/v0.0.3...v0.0.4) (2021-11-09)


### Bug Fixes

* added eror catching to index.ts ([c107abf](https://gitlab.com/yesodot/selenium/censorship-systems/blue-moon/blue-moon-real-time/commit/c107abf0abc2acad8fb084e0a987a1cc9c5fcf28))

### [0.0.3](https://gitlab.com/yesodot/selenium/censorship-systems/blue-moon/blue-moon-real-time/compare/v0.0.2...v0.0.3) (2021-09-14)

### [0.0.2](https://gitlab.com/yesodot/selenium/censorship-systems/blue-moon/blue-moon-real-time/compare/v0.0.1...v0.0.2) (2021-08-24)

### 0.0.1 (2021-08-10)


### Features

* add emit to resource socket on task updated ([d2461b9](https://gitlab.com/yesodot/selenium/censorship-systems/blue-moon/blue-moon-real-time/commit/d2461b95cd2b4675812a25cdc5e2eddc8a0d283a))
* create initial real time service & add changeScope event ([968ef2a](https://gitlab.com/yesodot/selenium/censorship-systems/blue-moon/blue-moon-real-time/commit/968ef2a854eb73fca418c64518c0ca39e225d4d1))
* emit events to sockets by scopes array ([086afa6](https://gitlab.com/yesodot/selenium/censorship-systems/blue-moon/blue-moon-real-time/commit/086afa6092d314bb181ba1f099e9fc22a72b3ec3))
* initialize service ([6c93e2f](https://gitlab.com/yesodot/selenium/censorship-systems/blue-moon/blue-moon-real-time/commit/6c93e2f2bec84607d310625b6c8730e5ba721182))
* update saving data flow and add unit tests ([f5bd97e](https://gitlab.com/yesodot/selenium/censorship-systems/blue-moon/blue-moon-real-time/commit/f5bd97e74b38ec1efe61b985425bf4d5776468dc))


### Bug Fixes

* message queue name ([a3cb2c2](https://gitlab.com/yesodot/selenium/censorship-systems/blue-moon/blue-moon-real-time/commit/a3cb2c2a7b12ac2885b0607cd9e54865f5c92871))
